package com.example.bluetooth1;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter bluetoothAdapter;
    private final int REQUEST_ENABLE_BT = 1;
    String listElement;
    BluetoothDevice targetDevice;
    BluetoothSocket btSocket;
    OutputStreamWriter writer;
    int ConnessioneOK=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // il dispositivo non supporta Bluetooth
            Toast.makeText(this, "Il dispositivo non supporta la connettività bluetooth", Toast.LENGTH_SHORT).show();
            finish();
        }

        Button btnTermina = (Button)findViewById(R.id.btnTermina);
        btnTermina.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        // se il bluetooth  è abilitato elenca i dipositivi accoppiati
        else elencaPairedDevices();

        super.onPostCreate(savedInstanceState);
    }

    private void elencaPairedDevices() {

        // insieme dei dispositivi associati
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        ListView listView1 = (ListView)findViewById(R.id.lvDispositivi);

        // elenca in un ArrayList i dispositivi associati
        ArrayList<String> arrayList1 = new ArrayList<>();
        for(BluetoothDevice pairedDevice : pairedDevices)
            arrayList1.add(pairedDevice.getName());

        // crea un ArrayAdapter per l'elenco dei dispositivi
        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<>(this, R.layout.paired_device_row, arrayList1);
        listView1.setAdapter(arrayAdapter1);

        // attiva un listener per il listView
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // individua un dispositivo della lista a cui inviare "Ciao Mondo!"
                listElement = (String)parent.getItemAtPosition(position);
                ConnessioneOK = 0;
                //sayHelloToDevice(listElement);
                Log.w("ciao",listElement);
                vaiASub(listElement);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // controlla l'esito di una richiesta di abilitazione del bluetooth
        if(requestCode == REQUEST_ENABLE_BT)

            // se l'utente ha risposto NO oppure non si è abilitato il bluetooth
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Bluetooth non abilitato", Toast.LENGTH_SHORT).show();
                finish();
            }
            else elencaPairedDevices();

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if ((ConnessioneOK==0)||(ConnessioneOK>3)) {
            try {
                btSocket.close();
                Toast.makeText(this, "Chiusura della comunicazione con il dispositivo", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                Toast.makeText(this, "Impossibile chiudere la comunicazione con il dispositivo", Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }

    public void vaiASub(String nomedisp){
        Intent i = new Intent(this,colorPick.class);
        i.putExtra("arduino", nomedisp);
        startActivity(i);
    }



}