#include <SoftwareSerial.h>
#include<stdio.h>
const int RXPin = 2;  // da collegare su TX di HC05
const int TXPin = 3;  // da collegare su RX di HC05
SoftwareSerial myBT = SoftwareSerial(RXPin, TXPin);//creo una nuova porta seriale via software
const int ritardo = 10;
int redPin= 9;
int greenPin = 10;
int bluePin = 11;
char msgChar;
int R, G, B;
char prova [11] = "000,000,015";
String indata;
void setup()
{
  pinMode(RXPin, INPUT);
  pinMode(TXPin, OUTPUT);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  myBT.begin(9600);
  Serial.begin(9600); 
  memset(prova, 0, sizeof(prova));
}

void loop(){
  while(myBT.available()){
    msgChar = char(myBT.read());
    //Serial.print(msgChar);
    if(msgChar!='\n'){
      indata+=msgChar;
    }
  }
  if(indata.length()>10){
    Serial.println(indata);
    indata.toCharArray(prova, 13);
    indata="";
    }
  char valoreR [3] ; 
  for(int i=0;i<3;i++){
    valoreR[i]=prova[i];  
  }
  valoreR[3]='\0';
  R = atoi(valoreR);
  char valoreG [3] ;

  for(int i=0;i<3;i++){
    valoreG[i]=prova[i+4];  
  }
  valoreG[3]='\0';
  G = atoi(valoreG);
  char valoreB [3] ;
  for(int i=0;i<3;i++){
    valoreB[i]=prova[i+8];
  }
  valoreB[3]='\0';
  B = atoi(valoreB);
  RGB_color(R,G,B);
  delay(ritardo);            
}

void RGB_color(int redValue, int greenValue, int blueValue)
 {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
